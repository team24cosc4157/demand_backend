import unittest
import Order

class Test_Order(unittest.TestCase):

# getters for Order instances down below -->
    def test_constructor(self):
        userOrder = Order.Order("Steve", "12345", "666 Doctor Lane", "Rx Delivery")
        assert userOrder.docName == "Steve"
        assert userOrder.deliveryNum == "12345"
        assert userOrder.deliveryAddress == "666 Doctor Lane"
        assert userOrder.serviceType == "Rx Delivery"

    def test_get_doc_name(self):
        userOrder = Order.Order("Steve", "12345", "666 Doctor Lane, Rx Delivery")
        assert userOrder.getDoctorName() == "Steve"

    def test_get_delivery_number(self):
        userOrder = Order.Order("Steve", "12345", "666 Doctor Lane, Rx Delivery")
        assert userOrder.getDeliveryNumber() == "12345"

    def test_get_delivery_address(self):
        userOrder = Order.Order("Steve", "12345", "666 Doctor Lane, Rx Delivery")
        assert userOrder.getDeliveryAddress() == "666 Doctor Lane"

    def test_get_service_type(self):
        userOrder = Order.Order("Steve", "12345", "666 Doctor Lane, Rx Delivery")
        assert userOrder.getServiceType() == "Rx Delivery"

# setters for Order instances down below -->
    def test_set_doctor_name(self):
        userOrder = Order.Order("Steve", "12345", "666 Doctor Lane, Rx Delivery")
        userOrder.setDoctorName("Bob")
        assert userOrder.setDoctorName() == "Bob"

    def test_set_delivery_number(self):
        userOrder = Order.Order("Steve", "12345", "666 Doctor Lane, Rx Delivery")
        userOrder.setDeliveryNumber("56789")
        assert userOrder.setDeliveryNumber() == "56789"

    def test_set_delivery_address(self):
        userOrder = Order.Order("Steve", "12345", "666 Doctor Lane, Rx Delivery")
        userOrder.setDeliveryAddress("6969 The Devils Street")
        assert userOrder.setDeliveryAddress() == "6969 The Devils Street"

    def test_set_service_type(self):
        userOrder = Order.Order("Steve", "12345", "666 Doctor Lane, Rx Delivery")
        userOrder.setServiceType("Donation Delivery")
        assert userOrder.setServiceType() == "Donation Delivery"


if __name__ == '__main__':
    unittest.main()
