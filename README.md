# README

### Summary

* Back end files for the demand-side server (demand.team24.softwareengineeringii.com).

### Navigation

* `Order.py`
    * Order object.
* `TestOrder.py`
    * Test of order object.
* `HTTPrequest_demand.py`
    * Receives web service requests and performs tasks accordingly (sending data to database, logging in user, etc.).

### Team members

* TM1 - Sydney Mack
* TM2 - Mariah Sager
* TM3 - Keldon Boswell
* TM4 - Colby Hayes
* TM5 - Karla Salto
