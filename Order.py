class Order:

    #Constructs a new Order instance
    def __init__(self, docName, deliveryNum, deliveryAddress, serviceType):
        self.docName = docName
        self.deliveryNum = deliveryNum
        self.deliveryAddress = deliveryAddress
        self.serviceType = serviceType

    # Return a formatted string instance of a Order Object
    def __str__(self):
        return self.docName + " " + self.rxNum + " " + self.deliveryAddress + " " + self.serviceType

    # Returns doctor name of a Order instance
    def getDoctorName(self):
        return self.docName

    # Returns the delivery number of a Order instance
    def getDeliveryNumber(self):
        return self.deliveryNum

    # Returns the delivery address of a Order instance
    def getDeliveryAddress(self):
        return self.deliveryAddress

    # Returns the service type of a Order instance
    def getServiceType(self):
        return self.serviceType

    # Sets the doctor name of a Order instance
    def setDoctorName(self, docName):
        self.docName = docName

    # Sets the delivery number of a Order instance
    def setDeliveryNumber(self, deliveryNum):
        self.deliveryNum = deliveryNum

    # Sets the email of a Order instance
    def setDeliveryAddress(self, deliveryAddress):
        self.deliveryAddress = deliveryAddress

    # Sets the service type of a Order instance
    def setServiceType(self, serviceType):
        self.serviceType = serviceType
